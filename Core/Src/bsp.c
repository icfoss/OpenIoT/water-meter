/**
  ******************************************************************************
  * @file    bsp.c
  * @author  MCD Application Team
  * @brief   manages the sensors on the application
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2018 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include <string.h>
#include <stdlib.h>
#include "hw.h"
#include "timeServer.h"
#include "bsp.h"
#include "MY_FLASH.h"
#if defined(LRWAN_NS1)
#include "lrwan_ns1_humidity.h"
#include "lrwan_ns1_pressure.h"
#include "lrwan_ns1_temperature.h"
#else  /* not LRWAN_NS1 */
#if defined(SENSOR_ENABLED)
#if defined (X_NUCLEO_IKS01A1)
#warning "Do not forget to select X_NUCLEO_IKS01A1 files group instead of X_NUCLEO_IKS01A2"
#include "x_nucleo_iks01a1_humidity.h"
#include "x_nucleo_iks01a1_pressure.h"
#include "x_nucleo_iks01a1_temperature.h"
#else  /* not X_NUCLEO_IKS01A1 */
#include "x_nucleo_iks01a2_humidity.h"
#include "x_nucleo_iks01a2_pressure.h"
#include "x_nucleo_iks01a2_temperature.h"
#endif  /* X_NUCLEO_IKS01A1 */
#endif  /* SENSOR_ENABLED */
#endif  /* LRWAN_NS1 */


/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
#define STSOP_LATTITUDE ((float) 43.618622 )
#define STSOP_LONGITUDE ((float) 7.051415  )
#define MAX_GPS_POS ((int32_t) 8388607  ) // 2^23 - 1
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/
/* Exported functions ---------------------------------------------------------*/

/* Private variables ---------------------------------------------------------*/
unsigned char a=0,f=0,co=0,tam=0;
long int Total;
long int Previous_value =16050;
long int Current_value=16050;

unsigned char x01=7,x1=4; //x1 will be set with the tens digit of the first reading
long int x=71;  //x is water reading in cubic meter,x1 is one point after decimal,x01 is two point after decimal.
unsigned char lit1=0;
unsigned char t1=0;
int j=0;
int k=0;


//uint32_t adrs=0x0800F000;
uint32_t adrs[]={0x0800F000,0x0800F010,0x0800F020,0x0800F030,0x0800F040,0x0800F050};
int l=0;
//  lit1=0;
//  a=0;
//  x01=7;
//  x1=4;
//  x=71;

uint32_t myTestWrite[]={};
#if defined(SENSOR_ENABLED) || defined (LRWAN_NS1)
void *HUMIDITY_handle = NULL;
void *TEMPERATURE_handle = NULL;
void *PRESSURE_handle = NULL;
#endif

void BSP_sensor_Read(sensor_t *sensor_data)
{
  /* USER CODE BEGIN 5 */
  float HUMIDITY_Value = 0;
  float TEMPERATURE_Value = 0;
  float PRESSURE_Value = 0;

#if defined(SENSOR_ENABLED) || defined (LRWAN_NS1)
  BSP_HUMIDITY_Get_Hum(HUMIDITY_handle, &HUMIDITY_Value);
  BSP_TEMPERATURE_Get_Temp(TEMPERATURE_handle, &TEMPERATURE_Value);
  BSP_PRESSURE_Get_Press(PRESSURE_handle, &PRESSURE_Value);
#endif
  sensor_data->humidity    = HUMIDITY_Value;
  sensor_data->temperature = TEMPERATURE_Value;
  sensor_data->pressure    = PRESSURE_Value;

  sensor_data->latitude  = (int32_t)((STSOP_LATTITUDE  * MAX_GPS_POS) / 90);
  sensor_data->longitude = (int32_t)((STSOP_LONGITUDE  * MAX_GPS_POS) / 180);
  sensor_data->Total1 = Total;
  /* USER CODE END 5 */
}

void  BSP_sensor_Init(void)
{
  /* USER CODE BEGIN 6 */

#if defined(SENSOR_ENABLED) || defined (LRWAN_NS1)
  /* Initialize sensors */
  BSP_HUMIDITY_Init(HTS221_H_0, &HUMIDITY_handle);
  BSP_TEMPERATURE_Init(HTS221_T_0, &TEMPERATURE_handle);
  BSP_PRESSURE_Init(PRESSURE_SENSORS_AUTO, &PRESSURE_handle);

  /* Enable sensors */
  BSP_HUMIDITY_Sensor_Enable(HUMIDITY_handle);
  BSP_TEMPERATURE_Sensor_Enable(TEMPERATURE_handle);
  BSP_PRESSURE_Sensor_Enable(PRESSURE_handle);
#endif
  /* USER CODE END 6 */
}
void init_gpio_interrupt()
{
    GPIO_InitTypeDef initStruct = {0};
    initStruct.Mode = GPIO_MODE_IT_FALLING;
    initStruct.Pull = GPIO_PULLDOWN;
    initStruct.Speed = GPIO_SPEED_HIGH;

//    HW_GPIO_Init(USER_BUTTON_GPIO_PORT, USER_BUTTON_PIN, &initStruct);
//    HW_GPIO_SetIrq(USER_BUTTON_GPIO_PORT, USER_BUTTON_PIN, 0, do_when_gpio_interrupted);
    HW_GPIO_Init(GPIOB, GPIO_PIN_12, &initStruct);
    HW_GPIO_SetIrq(GPIOB, GPIO_PIN_12, 0, switch2_interruptFunction);
    HW_GPIO_Init(GPIOB, GPIO_PIN_14, &initStruct);
    HW_GPIO_SetIrq(GPIOB, GPIO_PIN_14, 0, switch1_interruptFunction);
    }
//void do_when_gpio_interrupted()
//{
//  /* Prevent unused argument(s) compilation warning */
////  UNUSED(GPIO_Pin);/
//  PRINTF("callback\r\n");
//  /* NOTE: This function Should not be modified, when the callback is needed,
//           the HAL_GPIO_EXTI_Callback could be implemented in the user file
//   */
//  HAL_GPIO_DeInit(USER_BUTTON_GPIO_PORT, USER_BUTTON_PIN);
//}
void switch2_interruptFunction()
{
	PRINTF("callback12\r\n");
	if(f==2)
	  {
	    a++;
	    //co=0;
	    f=1;
	  }f=1;

	  HAL_GPIO_DeInit(GPIOB, GPIO_PIN_12);
	 eeprom();
	    GPIO_InitTypeDef initStruct = {0};
	    initStruct.Mode = GPIO_MODE_IT_FALLING;
	    initStruct.Pull = GPIO_PULLDOWN;
	    initStruct.Speed = GPIO_SPEED_HIGH;

	  HW_GPIO_Init(GPIOB, GPIO_PIN_14, &initStruct);
	  HW_GPIO_SetIrq(GPIOB, GPIO_PIN_14, 0, switch1_interruptFunction);
}
void switch1_interruptFunction()
{
	PRINTF("callback34\r\n");

    if(f==1)
      {
        f=2;
      }
    HAL_GPIO_DeInit(GPIOB, GPIO_PIN_14);
    GPIO_InitTypeDef initStruct = {0};
    initStruct.Mode = GPIO_MODE_IT_FALLING;
    initStruct.Pull = GPIO_PULLDOWN;
    initStruct.Speed = GPIO_SPEED_HIGH;
    HW_GPIO_Init(GPIOB, GPIO_PIN_12, &initStruct);
   	HW_GPIO_SetIrq(GPIOB, GPIO_PIN_12, 0, switch2_interruptFunction);
}

void eeprom()

    {
	PRINTF("okok\r\n",x01);
	HAL_Delay(1000);
       x01=a;

   t1=a;
  if(x01>9)
  {
      x1++;
      x01=0;a=0;
  }
  if(x1>9)
  {
      x++;
      x1=0;
  }

Total=(x*1000)+(x1*100)+(x01*10);




// MY_FLASH_SetSectorAddrs(480,0x0800F000);
//MY_FLASH_SetSectorAddrs(15,0x0800F000);
//if(j=0,j<=10,j++)
//{
myTestWrite[0]=(uint32_t*) Total;
j++;
MY_FLASH_WriteN(0,myTestWrite,1,DATA_TYPE_32);


//k+=4;

if(l<=6)
{
	PRINTF("ok123444\r\n");
MY_FLASH_SetSectorAddrs(15,adrs[l]);
l++;

//MY_FLASH_WriteN(0,myTestWrite,1,DATA_TYPE_32);
//l++;
}else
{
	l=0;
	MY_FLASH_SetSectorAddrs(15,0x0800F000);
	l++;
}
//k+=0x00000004;

//}
Current_value =Total;
PRINTF("Total = %d\n",Total);
    }
/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
