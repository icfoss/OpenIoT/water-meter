/**
  ******************************************************************************
  * @file    bsp.c
  * @author  MCD Application Team
  * @brief   manages the sensors on the application
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2018 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include <string.h>
#include <stdlib.h>
#include "hw.h"
#include "timeServer.h"
#include "bsp.h"
#include "util_console.h"
#include "lora.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
#define STSOP_LATTITUDE ((float) 43.618622 )
#define STSOP_LONGITUDE ((float) 7.051415  )
#define MAX_GPS_POS ((int32_t) 8388607  ) // 2^23 - 1
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/
/* Exported functions ---------------------------------------------------------*/

/* Private variables ---------------------------------------------------------*/
unsigned char a,f,co;
uint32_t Total;
uint32_t Total1;
extern LoraFlagStatus AppProcessRequest ;
extern TimerEvent_t TxTimer;
float BatteryAdcResult = 0;                   /*   adc reading for battery is stored in the variable  */
uint16_t BatteryLevel=0;                     /*    battery voltage   */

//

//long int Previous_value =16050;
//long int Current_value=16050;

//unsigned char x01=0,a=0;x1=0; //x1 will be set with the tens digit of the first reading
//long int x=0;  //x is water reading in cubic meter,x1 is one point after decimal,x01 is two point after decimal.
//unsigned char lit1=0;
//unsigned char t1=0;

unsigned char x01,x1; //x1 will be set with the tens digit of the first reading
long int x;  //x is water reading in cubic meter,x1 is one point after decimal,x01 is two point after decimal.
unsigned char lit1;
unsigned char t1;
int j=0;
int k=0;

uint32_t adrs[]={0x08080000,0x08080010,0x08080020,0x08080030,0x08080040,0x08080050};

int l=0;

#if defined(SENSOR_ENABLED) || defined (LRWAN_NS1)
void *HUMIDITY_handle = NULL;
void *TEMPERATURE_handle = NULL;
void *PRESSURE_handle = NULL;
#endif

void BSP_sensor_Read(sensor_t *sensor_data)
{

  /* USER CODE BEGIN 5 */
//  float HUMIDITY_Value = 0;
//  float TEMPERATURE_Value = 0;
//  float PRESSURE_Value = 0;

	//		Total=((x*1000)+(x1*100)+(x01*10));
//	HAL_FLASHEx_DATAEEPROM_Unlock();
//		    HAL_FLASHEx_DATAEEPROM_Program(FLASH_TYPEPROGRAMDATA_WORD, 0x08080008, a);
//		    HAL_FLASHEx_DATAEEPROM_Program(FLASH_TYPEPROGRAMDATA_WORD, 0x08080014, x1);
//		    HAL_FLASHEx_DATAEEPROM_Program(FLASH_TYPEPROGRAMDATA_WORD, 0x08080018, x);
//			HAL_FLASHEx_DATAEEPROM_Program(FLASH_TYPEPROGRAMDATA_WORD, 0x08080000, Total);
//			HAL_FLASHEx_DATAEEPROM_Program(FLASH_TYPEPROGRAMDATA_WORD, 0x08080000+4, Total);
//		HAL_FLASHEx_DATAEEPROM_Lock();


#if defined(SENSOR_ENABLED) || defined (LRWAN_NS1)
  BSP_HUMIDITY_Get_Hum(HUMIDITY_handle, &HUMIDITY_Value);
  BSP_TEMPERATURE_Get_Temp(TEMPERATURE_handle, &TEMPERATURE_Value);
  BSP_PRESSURE_Get_Press(PRESSURE_handle, &PRESSURE_Value);
#endif
//  sensor_data->humidity    = HUMIDITY_Value;
//  sensor_data->temperature = TEMPERATURE_Value;
//  sensor_data->pressure    = PRESSURE_Value;
//Total=99999999;
  sensor_data->latitude  = (int32_t)((STSOP_LATTITUDE  * MAX_GPS_POS) / 90);
  sensor_data->longitude = (int32_t)((STSOP_LONGITUDE  * MAX_GPS_POS) / 180);
  sensor_data->TotalWater = *(uint32_t *)0x08080004;

  /* USER CODE END 5 */
}

void  BSP_sensor_Init(void)
{
  /* USER CODE BEGIN 6 */

#if defined(SENSOR_ENABLED) || defined (LRWAN_NS1)
  /* Initialize sensors */
  BSP_HUMIDITY_Init(HTS221_H_0, &HUMIDITY_handle);
  BSP_TEMPERATURE_Init(HTS221_T_0, &TEMPERATURE_handle);
  BSP_PRESSURE_Init(PRESSURE_SENSORS_AUTO, &PRESSURE_handle);

  /* Enable sensors */
  BSP_HUMIDITY_Sensor_Enable(HUMIDITY_handle);
  BSP_TEMPERATURE_Sensor_Enable(TEMPERATURE_handle);
  BSP_PRESSURE_Sensor_Enable(PRESSURE_handle);
#endif
  /* USER CODE END 6 */
}

/*GPIO INIT */

void init_gpio_interrupt()
{

	GPIO_InitTypeDef initStruct = {0};
    initStruct.Mode = GPIO_MODE_IT_FALLING;
    initStruct.Pull = GPIO_PULLUP;
    initStruct.Speed = GPIO_SPEED_HIGH;

    HW_GPIO_Init(GPIOB, GPIO_PIN_8, &initStruct);
    HW_GPIO_SetIrq(GPIOB, GPIO_PIN_8, 1, switch1_interruptFunction);
    HW_GPIO_Init(GPIOB, GPIO_PIN_9, &initStruct);
  	HW_GPIO_SetIrq(GPIOB, GPIO_PIN_9, 1, switch2_interruptFunction);
//    HW_GPIO_Init(GPIOB, GPIO_PIN_14, &initStruct);
//    HW_GPIO_SetIrq(GPIOB, GPIO_PIN_14, 0, switch1_interruptFunction);
    }

/*GPIO INIT */

void init_gpio_tamp_interrupt()
{
	    GPIO_InitTypeDef initStruct = {0};
	    initStruct.Mode = GPIO_MODE_IT_FALLING;
	    initStruct.Pull = GPIO_PULLUP;
	    initStruct.Speed = GPIO_SPEED_HIGH;

	    HW_GPIO_Init(GPIOB, GPIO_PIN_14, &initStruct);
	    HW_GPIO_SetIrq(GPIOB, GPIO_PIN_14, 0, tamp_interruptFunction);
}

/*REED SWITCH INTERRUPT 2 */
/**
 * Created on: Oct 20, 2020
 * Last Edited: Nov 1, 2020
 * Author: Arun, Ajmi-- ICFOSS
 *
 * @brief  read the interrupt pin status
 * @param  none
 *
 **/
void switch2_interruptFunction()
{
//	GPIO_PinState tam = HAL_GPIO_ReadPin(GPIOB,GPIO_PIN_7);
	if(f==2)
		  {
		    a++;
		    //co=0;
		    f=1;
		  }f=1;
	    HAL_GPIO_DeInit(GPIOB, GPIO_PIN_9);
	    eeprom();
	    GPIO_InitTypeDef initStruct = {0};
	    initStruct.Mode = GPIO_MODE_IT_FALLING;
	    initStruct.Pull = GPIO_PULLUP;
	    initStruct.Speed = GPIO_SPEED_HIGH;

	    HW_GPIO_Init(GPIOB, GPIO_PIN_8, &initStruct);
	    HW_GPIO_SetIrq(GPIOB, GPIO_PIN_8, 1, switch1_interruptFunction);

}

/*REED SWITCH INTERRUPT 1 */
/**
 * Created on: Oct 20, 2020
 * Last Edited: Nov 1, 2020
 * Author: Arun, Ajmi-- ICFOSS
 *
 * @brief  read the interrupt pin status
 * @param  none
 *
 **/

void switch1_interruptFunction() {
//GPIO_PinState tam = HAL_GPIO_ReadPin(GPIOB,GPIO_PIN_7);
//PRINTF("TAMP = %d\n",tam);
if(f==1)
      {
        f=2;
      }
	HAL_GPIO_DeInit(GPIOB, GPIO_PIN_8);
	GPIO_InitTypeDef initStruct = {0};
	    initStruct.Mode = GPIO_MODE_IT_FALLING;
	    initStruct.Pull = GPIO_PULLUP;
	    initStruct.Speed = GPIO_SPEED_HIGH;

	    HW_GPIO_Init(GPIOB, GPIO_PIN_9, &initStruct);
	    HW_GPIO_SetIrq(GPIOB, GPIO_PIN_9, 1, switch2_interruptFunction);
}


/*WATER FLOW CALCULATION AND EEPROM DATE STORAGE*/
/**
 * Created on: Oct 21, 2020
 * Last Edited: Nov 3 , 2020
 * Author: Arun 
 *
 * @brief  logical calclation of the water flow
 * @param  none
 *
 **/
void eeprom()

    {

       x01=a;                //10 L

       PRINTF("x01 = %d\n",x01);
   t1=a;

  if(a>9)
  {
      x1++;
      x01=0;a=0; //100 L

      PRINTF("x1 = %d\n",x1);

  }
  if(x1>9)
  {
      x++;   //1000 L
      x1=0;

      PRINTF("x = %d\n",x);

  }

Total=((x*1000)+(x1*100)+(x01*10));

	/*EEPROM DATA STORING*/

	HAL_FLASHEx_DATAEEPROM_Unlock();
	    HAL_FLASHEx_DATAEEPROM_Program(FLASH_TYPEPROGRAMDATA_WORD, 0x08080008, a);
	    HAL_FLASHEx_DATAEEPROM_Program(FLASH_TYPEPROGRAMDATA_WORD, 0x08080014, x1);
	    HAL_FLASHEx_DATAEEPROM_Program(FLASH_TYPEPROGRAMDATA_WORD, 0x08080018, x);
		HAL_FLASHEx_DATAEEPROM_Program(FLASH_TYPEPROGRAMDATA_WORD, 0x08080000, Total);
		HAL_FLASHEx_DATAEEPROM_Program(FLASH_TYPEPROGRAMDATA_WORD, 0x08080000+4, Total);
	HAL_FLASHEx_DATAEEPROM_Lock();

PRINTF("Total = %d\n",Total);
PRINTF("x01 = %d\n",x01);
PRINTF("x1 = %d\n",x1);
PRINTF("x = %d\n",x);
    }

/*BATTERY VOLTAGE CALCULATION*/
/**
 * Created on: Oct 28, 2020
 * Last Edited: Nov 15 2020
 * Author: Arun 
 *
 * @brief  read external battery voltage connected to analog channel 4 through a voltage divider
 * @param  none
 * @retval battery voltage level
 *
 **/
uint16_t batterylife()
{
    HAL_GPIO_WritePin(GPIOB, GPIO_PIN_2, GPIO_PIN_RESET);//for battery
	BatteryAdcResult = HW_AdcReadChannel(ADC_CHANNEL_4);
  PRINTF("BatteryAdcResult= %.1f\r\n",BatteryAdcResult);
	BatteryAdcResult=(BatteryAdcResult*3.3)/4096;
	BatteryAdcResult=(BatteryAdcResult*2)*10;//*10 to convert to int from float
	BatteryLevel=(uint16_t)BatteryAdcResult;
	return BatteryLevel;
}

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
